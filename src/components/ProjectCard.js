import { Col } from "react-bootstrap";

export const ProjectCard = ({ title, description, imgUrl, link }) => {
  return (
    <Col size={12} sm={6} md={4}>
      <a href={link} target="__blank" id="card-link">
      <div className="proj-imgbx">
        <img src={imgUrl} alt="Project Display" height={500} width={300} />
        <div className="proj-txtx">
          <h4>{title}</h4>
          <span className="p-4">{description}</span>
        </div>
      </div>
      </a>
    </Col>
  )
}
